


function deleteItem(url) {
    if ($('#deleteForm').length > 0) {
        if (confirm('Are you sure you want to delete this?')) {
            $('#deleteForm').attr('action', url);
            $('#deleteForm').submit();
        }
    }
}

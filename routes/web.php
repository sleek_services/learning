<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'account', 'namespace' => 'Backend', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index');

    Route::resource('students', 'StudentController');
    Route::resource('teacher', 'TeacherController');
    Route::resource('static-page', 'StaticPageController');
    Route::resource('settings', 'SettingController');
    Route::get('user/profile', function () {
        // Uses Auth Middleware
    });
});
Route::group(['prefix' => 'teacher', 'namespace' => 'Teacher', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index');
    Route::get('user/profile', function () {
        // Uses Auth Middleware
    });
});
Route::group(['prefix' => 'student', 'namespace' => 'Student', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index');
    Route::get('user/profile', function () {
        // Uses Auth Middleware
    });
});

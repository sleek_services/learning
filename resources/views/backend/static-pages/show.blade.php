@extends('layouts.admin')
@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Complete Information for {{$static_page->title}}</h3>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <td class="text-left col-lg-3"><strong>ID</strong></td>
                    <td class="text-left">{{$static_page->id}}</td>
                </tr>
                <tr>
                    <td class="text-left col-lg-3"><strong>Title</strong></td>
                    <td class="text-left">{{$static_page->title}}</td>
                </tr>
                <tr>
                    <td class="text-left col-lg-3"><strong>URL Key</strong></td>
                    <td class="text-left">{{$static_page->url_key}}</td>
                </tr>
                <tr>
                    <td colspan="2" class="text-left">
                        {!!$static_page->content!!}
                    </td>
                </tr>
                <tr>
                    <td class="text-left col-lg-3"><strong>Created By</strong></td>
                    <td class="text-left">{{$static_page->createdBy->first_name}}</td>
                </tr>
                <tr>
                    <td class="text-left col-lg-3"><strong>Updated By</strong></td>
                    <td class="text-left">{{@$static_page->updatedBy->first_name}}</td>
                </tr>
                <tr>
                    <td class="text-left col-lg-3"><strong>Status</strong></td>
                    <td class="text-left">{{empty($static_page->status) ? 'Inactive':'Active'}}</td>
                </tr>
                <tr>
                    <td class="text-left col-lg-3"><strong>Meta Keywords</strong></td>
                    <td class="text-left">{{$static_page->meta_keywords}}</td>
                </tr>
                <tr>
                    <td class="text-left col-lg-3"><strong>Meta Description</strong></td>
                    <td class="text-left">{{$static_page->meta_description}}</td>
                </tr>
                <tr>
                    <td class="text-left col-lg-3"><strong>Created on</strong></td>
                    <td class="text-left">{{$static_page->created_at->diffForHumans()}}</td>
                </tr>
                <tr>
                    <td class="text-left col-lg-3"><strong>Updated On</strong></td>
                    <td class="text-left">{{$static_page->updated_at->diffForHumans()}}</td>
                </tr>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@stop
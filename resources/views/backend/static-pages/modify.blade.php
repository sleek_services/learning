@extends('layouts.cp')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Static Page
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Setting</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <form method="POST" action="{{admin_url('static-page'. empty($static_page) ? '': '/' . $static_page->id)}}" enctype="multipart/form-data" class="form-horizontal">

                <fieldset>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">@if(empty($static_page))Add a new Page @else Update Page @endif</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="title" class="col-lg-2 control-label">Title</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="title" name="title" value="{{old('title', @$static_page->title)}}" placeholder="Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-lg-2 control-label">Content</label>
                                <div class="col-lg-10">
                                    <textarea id="content" name="content" class="form-control">{{old('content', @$static_page->content)}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="meta_keywords" class="col-lg-2 control-label">Meta Keywords</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" value="{{old('meta_keywords', @$static_page->meta_keywords)}}" placeholder="Meta Keywords">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="meta_description" class="col-lg-2 control-label">Meta Description</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="meta_description" name="meta_description" value="{{old('meta_description', @$static_page->meta_description)}}" placeholder="Meta Description">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Status</label>
                                <div class="col-lg-10">
                                    <div class="row">
                                        <div class="col-xs-6 col-md-4 col-lg-3">
                                            <select id="status" name="status" class="form-control">
                                                <?php $status = old('status'); ?>
                                                <option value="0" @if(empty($status)) selected="selected" @endif>InActive</option>
                                                <option value="1" @if(!empty($status)) selected="selected" @endif>Active</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    @if(!empty($static_page)) {!! method_field('PUT') !!} @endif
                                    {!! csrf_field() !!}
                                    <button type="submit" class="btn btn-primary">@if(empty($static_page)) Create @else Update @endif</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </fieldset>
            </form>
            <div class="clearfix"></div>

        </section>
        <!-- /.content -->
    </div>

@endsection

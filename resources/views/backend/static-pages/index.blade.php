@extends('layouts.cp')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Setting
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Setting</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">You can manage all static pages here</h3>
                </div>
                <div class="box-body">
                    @if (sizeof($static_pages))
                        <div class="table-responsive">
                            <table class="table table-striped table-hover col-lg-12">
                                <tr>
                                    <th class="col-md-1">ID</th>
                                    <th class="col-md-4 text-left">Title</th>
                                    <th>Status</th>
                                    <th>Updated By</th>
                                    <th>Created at</th>
                                    <th>Updated at</th>
                                    <th class="col-md-1 text-center">Options</th>
                                </tr>
                                @foreach($static_pages as $static_page)
                                    <tr>
                                        <td>{{$static_page->id}}</td>
                                        <td class="text-left"><a href="">{{$static_page->title}}</a></td>
                                        <td>@if($static_page->status=='1') Active @else Inactive @endif</td>
                                        <td><a href="users/"></a></td>
                                        <td>{{$static_page->created_at->diffForHumans()}}</td>
                                        <td>{{$static_page->updated_at->diffForHumans()}}</td>
                                        <td class="text-center">
                                            <a title="Modify" href="{{Request::url()}}/{{$static_page->id}}/edit" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>
                                            <a title="Delete" href="javascript:;" onclick="deleteItem('{{Request::url()}}/{{$static_page->id}}');" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="text-center">{!! $static_pages->render() !!}</div>
                        <form id="deleteForm" method="POST" action="">
                            {!! method_field('DELETE') !!}
                            {!! csrf_field() !!}
                        </form>
                    @else
                        Records not found.
                    @endif

                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </section>
        <!-- /.content -->
    </div>


@endsection
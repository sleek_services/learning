@extends('layouts.cp')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Setting
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Setting</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">You can manage all users here</h3>
                </div>
                <div class="box-body">
                    @if (sizeof($users))
                        <div class="table-responsive">
                            <table class="table table-striped table-hover col-lg-12">
                                <tr>
                                    <th class="col-md-1">ID</th>
                                    <th class="col-md-2 text-left">Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Created at</th>
                                    <th>Updated at</th>
                                    <th class="col-md-2 text-center">Options</th>
                                </tr>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td class="text-left"><a href="{{Request::url()}}/{{$user->id}}" title="View Detail">{{$user->name}}</a></td>
                                        <td>{{$user->email}}</td>
                                        <td>@if($user->status=='1') Active @else Inactive @endif</td>
                                        <td>{{$user->created_at}}</td>
                                        <td>{{$user->updated_at}}</td>
                                        <td class="text-center">
                                            <a title="Modify" href="{{Request::url()}}/{{$user->id}}/edit" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>
                                            <a title="Delete" href="javascript:;" onclick="deleteItem('{{Request::url()}}/{{$user->id}}');" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="text-center">{!! $users->render() !!}</div>
                        <form id="deleteForm" method="POST" action="">
                            {!! method_field('DELETE') !!}
                            {!! csrf_field() !!}
                        </form>
                    @else
                        Records not found.
                    @endif

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
@endsection
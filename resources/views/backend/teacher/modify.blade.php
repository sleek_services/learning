@extends('layouts.cp')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add a new User
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">User</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <form method="post" action="{{admin_url('teacher'.(empty($user) ? '': '/' . $user->id))}}"
                  enctype="multipart/form-data" class="form-horizontal">
                <fieldset>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">@if(empty($user))Add a new User @else Update User @endif</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-lg-2 control-label">Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="name" name="name"
                                           value="{{old('name', @$user->name)}}" placeholder="Name">
                                    @if ($errors->has('name'))
                                        <span class="help-block text-left">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                <label for="mobile_number" class="col-lg-2 control-label">Mobile Number</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="mobile_number" name="mobile_number"
                                           value="{{old('mobile_number', @$user->mobile_number)}}" placeholder="Mobile Number">
                                    @if ($errors->has('mobile_number'))
                                        <span class="help-block text-left">
                                <strong>{{ $errors->first('mobile_number') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-10">
                                    <input type="email" class="form-control" id="email" name="email"
                                           value="{{old('email', @$user->email)}}" placeholder="Email">
                                    @if ($errors->has('email'))
                                        <span class="help-block text-left">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-lg-2 control-label">Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" id="password" name="password" autocomplete="off"
                                           @if(!empty($user))placeholder="Leave blank to keep current password" @else placeholder="Password" @endif>
                                    @if ($errors->has('password'))
                                        <span class="help-block text-left">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="re-password" class="col-lg-2 control-label">Repeat Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" id="re-password" name="re-password"
                                           autocomplete="off" placeholder="Repeat Password">
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('profile_image') ? ' has-error' : '' }}">
                                <label for="profile_image" class="col-lg-2 control-label">Profile Picture</label>
                                <div class="col-lg-10">
                                    @if(!empty($user->profile_image))

                                        <img src="{{asset('uploads/users/'.$user->profile_image)}}" alt="" width="120" height="80">

                                    @endif
                                    <input type="file" id="profile_image" name="profile_image">
                                        @if ($errors->has('profile_image'))
                                            <span class="help-block text-left">
                                <strong>{{ $errors->first('profile_image') }}</strong>
                            </span>
                                        @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                                <label class="col-lg-2 control-label">Status</label>
                                <div class="col-lg-10">
                                    <div class="row">
                                        <div class="col-xs-6 col-md-4 col-lg-3">
                                            <select id="status" name="status" class="form-control">
                                                <?php $status = old('status'); ?>
                                                <option value="0" @if(empty($status)) selected="selected" @endif>InActive
                                                </option>
                                                <option value="1" @if(!empty($status)) selected="selected" @endif>Active
                                                </option>
                                            </select>
                                            @if ($errors->has('status'))
                                                <span class="help-block text-left">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    @if(!empty($user)) {!! method_field('PUT') !!} @endif
                                    {!! csrf_field() !!}
                                    <button type="submit" class="btn btn-primary">@if(empty($user)) Create @else
                                            Update @endif</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </fieldset>
            </form>
            <div class="clearfix"></div>

        </section>
        <!-- /.content -->
    </div>




@endsection
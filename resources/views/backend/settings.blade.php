@extends('layouts.cp')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Setting
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Setting</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Please be advised: Any changes made here will affect the live website.</h3>
                </div>
                <div class="box-body">

                    <form method="post" action="{{url('/account/settings')}}">
                        {!! csrf_field() !!}
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <tr>
                                    <th class="col-md-2 text-left">Setting</th>
                                    <th>Value</th>
                                    <th>Last updated at</th>
                                </tr>

                                <tr>
                                    <td class="text-left">{{$site_title->setting_name}}</td>
                                    <td><input class="form-control" type="text" id="site_title" name="site_title"
                                               value="{{$site_title->setting_value}}"></td>
                                    <td>{{$site_title->updated_at}}</td>
                                </tr>

                                <tr>
                                    <td class="text-left">{{$results_per_page->setting_name}}</td>
                                    <td><select id="{{ $results_per_page->setting_key }}"
                                                name="{{ $results_per_page->setting_key }}" class="form-control">
                                            <option value="20"
                                                    @if($results_per_page->setting_value == 20) selected="selected" @endif>
                                                20
                                            </option>
                                            <option value="50"
                                                    @if($results_per_page->setting_value == 50) selected="selected" @endif>
                                                50
                                            </option>
                                            <option value="80"
                                                    @if($results_per_page->setting_value == 80) selected="selected" @endif>
                                                80
                                            </option>
                                            <option value="100"
                                                    @if($results_per_page->setting_value == 100) selected="selected" @endif>
                                                100
                                            </option>
                                        </select></td>
                                    <td>{{$results_per_page->updated_at}}</td>
                                </tr>

                                <tr>
                                    <td class="text-left">{{$meta_keywords->setting_name}}</td>
                                    <td><input class="form-control" type="text" id="meta_keywords" name="meta_keywords"
                                               value="{{$meta_keywords->setting_value}}"></td>
                                    <td>{{$meta_keywords->updated_at}}</td>
                                </tr>

                                <tr>
                                    <td class="text-left">{{$meta_description->setting_name}}</td>
                                    <td><input class="form-control" type="text" id="meta_description"
                                               name="meta_description" value="{{$meta_description->setting_value}}">
                                    </td>
                                    <td>{{$meta_description->updated_at}}</td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td class="text-left" colspan="3">
                                        <button type="submit" class="btn btn-info">Update</button>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </form>


                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </section>
        <!-- /.content -->
    </div>



@endsection
@extends('layouts.cp')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Setting
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Setting</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Complete Information for {{$user->name}}</h3>
                </div>
                <div class="box-body">
                    <div class="col-lg-4 text-center vcenter">
                        <img src="{{asset('uploads/users/'.$user->profile_image)}}" alt="" width="180" height="220">
                    </div>
                    <div class="table-responsive col-lg-8">

                        <table class="table">
                            <tr>
                                <td class="text-left col-lg-3"><strong>ID</strong></td>
                                <td class="text-left">{{$user->id}}</td>
                            </tr>
                            <tr>
                                <td class="text-left col-lg-3"><strong>Name</strong></td>
                                <td class="text-left">{{$user->name}}</td>
                            </tr>

                            <tr>
                                <td class="text-left col-lg-3"><strong>Email</strong></td>
                                <td class="text-left">{{$user->email}}</td>
                            </tr>
                            <tr>
                                <td class="text-left col-lg-3"><strong>Status</strong></td>
                                <td class="text-left">{{empty($user->status) ? 'Inactive':'Active'}}</td>
                            </tr>
                            <tr>
                                <td class="text-left col-lg-3"><strong>Created on</strong></td>
                                <td class="text-left">{{$user->created_at}}</td>
                            </tr>
                            <tr>
                                <td class="text-left col-lg-3"><strong>Updated On</strong></td>
                                <td class="text-left">{{$user->updated_at}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>



@endsection
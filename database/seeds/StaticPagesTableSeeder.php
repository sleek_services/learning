<?php

use Illuminate\Database\Seeder;

class StaticPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('static_pages')->insert([
            [
                'created_by' => 1,
                'updated_by' => 1,
                'title' => 'About Us',
                'url_key' => 'about',
                'content' => 'About Us...',
                'created_at' => '2016-12-24 06:33:54',
                'updated_at' => '2016-12-26 06:33:54',
                'status' => 1
            ],
            [
                'created_by' => 1,
                'updated_by' => 1,
                'title' => 'FAQ',
                'url_key' => 'faq',
                'content' => 'FAQ...',
                'created_at' => '2016-12-24 06:33:54',
                'updated_at' => '2016-12-26 06:33:54',
                'status' => 1
            ],
            [
                'created_by' => 1,
                'updated_by' => 1,
                'title' => 'Contact',
                'url_key' => 'contact',
                'content' => 'Contact...',
                'created_at' => '2016-12-24 06:33:54',
                'updated_at' => '2016-12-26 06:33:54',
                'status' => 1
            ],
            [
                'created_by' => 1,
                'updated_by' => 1,
                'title' => 'Terms and Conditions',
                'url_key' => 'terms-conditions',
                'content' => 'Terms...',
                'created_at' => '2016-12-24 06:33:54',
                'updated_at' => '2016-12-26 06:33:54',
                'status' => 1
            ],
            [
                'created_by' => 1,
                'updated_by' => 1,
                'title' => 'Privacy Policy',
                'url_key' => 'privacy-policy',
                'content' => 'Privacy Policy...',
                'created_at' => '2016-12-24 06:33:54',
                'updated_at' => '2016-12-26 06:33:54',
                'status' => 1
            ],
            [
                'created_by' => 1,
                'updated_by' => 1,
                'title' => 'All Cities',
                'url_key' => 'all-cities',
                'content' => 'All Cities...',
                'created_at' => '2016-12-24 06:33:54',
                'updated_at' => '2016-12-26 06:33:54',
                'status' => 1
            ],
            [
                'created_by' => 1,
                'updated_by' => 1,
                'title' => 'Help and Support',
                'url_key' => 'help-and-support',
                'content' => 'Help and Support...',
                'created_at' => '2016-12-24 06:33:54',
                'updated_at' => '2016-12-26 06:33:54',
                'status' => 1
            ],
            [
                'created_by' => 1,
                'updated_by' => 1,
                'title' => 'Advertise With Us',
                'url_key' => 'advertise-with-us',
                'content' => 'Advertise With Us...',
                'created_at' => '2016-12-24 06:33:54',
                'updated_at' => '2016-12-26 06:33:54',
                'status' => 1
            ],
        ]);
    }
}

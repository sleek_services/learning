<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Sleek',
                'role_id' => 1,
                'mobile_number' => '+1-',
                'email' => 'admin@admin.com',
                'password' => bcrypt('admin123'),
                'status' => 1,
            ],
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('updated_by')->unsigned()->nullable();
            $table->string('setting_name');
            $table->string('setting_key')->unique();
            $table->string('setting_value')->nullable();
            $table->boolean('is_readonly')->default(1)->index();
            $table->timestamps();

            $table->foreign('updated_by')->references('id')->on('users')->onUpdate('cascade')->onDelete('SET NULL');
        });

        DB::table('settings')->insert([
            [
                'setting_key' => 'site_title',
                'setting_name' => 'Site Title',
                'setting_value' => 'Learning',
                'is_readonly' => '0',
            ],
            [
                'setting_key' => 'results_per_page',
                'setting_name' => 'Results Per Page',
                'setting_value' => '20',
                'is_readonly' => '0',
            ],
            [
                'setting_key' => 'meta_keywords',
                'setting_name' => 'Meta Keywords',
                'setting_value' => '',
                'is_readonly' => '0',
            ],
            [
                'setting_key' => 'meta_description',
                'setting_name' => 'Meta Description',
                'setting_value' => '',
                'is_readonly' => '0',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}

<?php

return [
    'ADMIN' => '/superpanel/',
    'UPLOAD_PATH' => public_path() . '/uploads/',
    'UPLOADS' => '/uploads/',
    'CURRENCY' => '₹',
    'FILE_MANAGER' => '/filemanager/index.html',
];

<?php

function admin_url($url='') {
    return url('/account/'.$url);
}
function teacher_url($url='') {
    return url('/teacher/'.$url);
}
function student_url($url='') {
    return url('/student/'.$url);
}
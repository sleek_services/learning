<?php

namespace App\Http\Controllers\Backend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id', 'desc')->paginate(15);
        view()->share('page_title', "View Users");
        return view('backend.users.index', ['users' => $users]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$data['roles'] = \App\Role::all();
        view()->share('page_title', 'Add a new User');
        view()->share('breadcrumbs', [['url' => config('constants.ADMIN') . 'users', 'title' => 'Users']]);
        return view('backend.users.modify');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:20',
            'email' => 'required|email|unique:users|min:10|max:255',
            'password' => 'required|min:8|max:255|same:re-password',
            're-password' => 'required|min:8|max:255',
            'profile_image' => 'image',
            'status' => 'required|boolean',
        ];
        $this->validate($request, $rules);
        if ($request->hasFile('profile_image')) {
            $filename = 'image-' . time() . rand(99, 199) . '.' . $request->file('profile_image')->getClientOriginalExtension();
            $request->file('profile_image')->move(config('constants.UPLOAD_PATH').'users', $filename);
        }
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
       // $user->role_id = $request->input('role_id');
        $user->mobile_number = $request->input('mobile_number');
        $user->password = bcrypt($request->input('password'));
        if (isset($filename))
            $user->profile_image = $filename;
        $user->status = $request->input('status');
        $user->save();
        return redirect(url('account/users'))->with('success', 'Your record has been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $data['user'] = $user;
        view()->share('page_title', 'User Information');
        view()->share('breadcrumbs', [['url' => config('constants.ADMIN') . 'users', 'title' => 'Users']]);
        return view('backend.users.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::findOrFail($id);
        view()->share('page_title', 'Modify User');
        view()->share('breadcrumbs', [['url' => config('constants.ADMIN') . 'users', 'title' => 'Users']]);
        return view('backend.users.modify', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {dd('sss');
        $rules = [
            'name' => 'required|max:20',
            'profile_image' => 'image',
            'status' => 'required|boolean',
        ];
        $user = User::findOrFail($id);
        if ($request->input('password') != '') {
            $rules['password'] = 'required|min:8|max:255|same:re-password';
            $rules['re-password'] = 'required|min:8|max:255';
        }

        $this->validate($request, $rules);
        if ($request->hasFile('profile_image')) {
            if (!empty($user->profile_image)) {
                $file = config('constants.UPLOAD_PATH').'users' . $user->profile_image;
                if (file_exists($file))
                    unlink($file);
            }

            $filename = 'image-' . time() . rand(99, 199) . '.' . $request->file('profile_image')->getClientOriginalExtension();
            $request->file('profile_image')->move(config('constants.UPLOAD_PATH').'users', $filename);
        }
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->mobile_number = $request->input('mobile_number');
        if ($request->input('password') != '') {
            $user->password = bcrypt($request->input('password'));
        }
        if (isset($filename))
            $user->profile_image = $filename;
        $user->status = empty($request->input('status')) ? 0 : 1;
        $user->save();
        return redirect(url('account/users/'))->with('success', 'Your record has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $file = config('constants.UPLOAD_PATH') . $user->profile_image;
        if(!empty($user->profile_image)){
            if (file_exists($file))
                unlink($file);
        }
        $user->delete();
        return redirect(url('account/users/'))->with('success', 'Your record has been Deleted successfully.');
    }
}

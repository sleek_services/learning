<?php

namespace App\Http\Controllers\Backend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role_id','=' ,2)->orderBy('id', 'desc')->paginate(15);
        view()->share('page_title', "View Teachers");
        return view('backend.teacher.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('page_title', 'Add a new Teacher');
        view()->share('breadcrumbs', [['url' => config('constants.ADMIN') . 'students', 'title' => 'Students']]);
        return view('backend.teacher.modify');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:20',
            'email' => 'required|email|unique:users|min:10|max:255',
            'password' => 'required|min:8|max:255|same:re-password',
            're-password' => 'required|min:8|max:255',
            'mobile_number' => 'required',
            'profile_image' => 'required|image',
            'status' => 'required|boolean',
        ];
        $this->validate($request, $rules);
        if ($request->hasFile('profile_image')) {
            $filename = 'image-' . time() . rand(99, 199) . '.' . $request->file('profile_image')->getClientOriginalExtension();
            $request->file('profile_image')->move(config('constants.UPLOAD_PATH').'users', $filename);
        }
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role_id = 2;
        $user->mobile_number = $request->input('mobile_number');
        $user->password = bcrypt($request->input('password'));
        if (isset($filename))
            $user->profile_image = $filename;
        $user->status = $request->input('status');
        $user->save();
        return redirect(admin_url('teacher'))->with('success', 'Your record has been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $data['user'] = $user;
        view()->share('page_title', 'User Information');
        view()->share('breadcrumbs', [['url' => config('constants.ADMIN') . 'users', 'title' => 'Users']]);
        return view('backend.student.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::findOrFail($id);
        view()->share('page_title', 'Modify User');
        view()->share('breadcrumbs', [['url' => config('constants.ADMIN') . 'users', 'title' => 'Users']]);
        return view('backend.teacher.modify', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:20',
            'status' => 'required|boolean',
        ];
        $user = User::findOrFail($id);
        if ($request->input('password') != '') {
            $rules['password'] = 'required|min:8|max:255|same:re-password';
            $rules['re-password'] = 'required|min:8|max:255';
        }

        $this->validate($request, $rules);
        if ($request->hasFile('profile_image')) {
            if (!empty($user->profile_image)) {
                $file = config('constants.UPLOAD_PATH').'users/' . $user->profile_image;
                if (file_exists($file))
                    unlink($file);
            }

            $filename = 'image-' . time() . rand(99, 199) . '.' . $request->file('profile_image')->getClientOriginalExtension();
            $request->file('profile_image')->move(config('constants.UPLOAD_PATH').'users', $filename);
        }
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role_id = 2;
        $user->mobile_number = $request->input('mobile_number');
        if ($request->input('password') != '') {
            $user->password = bcrypt($request->input('password'));
        }
        if (isset($filename))
            $user->profile_image = $filename;
        $user->status = $request->input('status');
        $user->save();
        return redirect(admin_url('teacher/'))->with('success', 'Your record has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $file = config('constants.UPLOAD_PATH').'teachers' . $user->profile_image;
        if(!empty($user->profile_image)){
            if (file_exists($file))
                unlink($file);
        }
        $user->delete();
        return redirect(admin_url('teacher/'))->with('success', 'Your record has been Deleted successfully.');
    }
}

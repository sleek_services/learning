<?php

namespace App\Http\Controllers\Backend;

use App\StaticPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class StaticPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $static_pages = StaticPage::orderBy('id', 'desc')->paginate(15);
        view()->share('page_title', "View Static Pages");
        return view('backend.static-pages.index', ['static_pages' => $static_pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('page_title', 'Add a new Page');
        view()->share('breadcrumbs', [['url' => config('constants.ADMIN').'static-pages', 'title' => 'Static Pages']]);
        return view('backend.static-pages.modify');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'content' => 'required',
            'meta_keywords' => 'max:255',
            'meta_description' => 'max:255',
            'status' => 'required|boolean',
        ];

        $this->validate($request, $rules);

        $static_page = new StaticPage;
        $static_page->created_by = Auth::user()->id;
        $static_page->updated_by = Auth::user()->id;
        $static_page->title = $request->input('title');
        $static_page->content = $request->input('content');
        $static_page->meta_keywords = $request->input('meta_keywords');
        $static_page->meta_description = $request->input('meta_description');
        $static_page->status = empty($request->input('status')) ? 0 : 1;
        $static_page->save();

        return redirect(config('constants.ADMIN') . 'static-pages')->with('success', 'Your page has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['static_page'] = StaticPage::findOrFail($id);
        view()->share('page_title', 'Modify Page');
        view()->share('breadcrumbs', [['url' => config('constants.ADMIN').'static-pages', 'title' => 'Static Pages']]);
        return view('backend.static-pages.modify', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {dd('ss');
        $rules = [
            'title' => 'required|max:255',
            'content' => 'required',
            'meta_keywords' => 'max:255',
            'meta_description' => 'max:255',
            'status' => 'required|boolean',
        ];
        $this->validate($request, $rules);

        $static_page = StaticPage::findOrFail($id);
        $static_page->updated_by = Auth::user()->id;
        $static_page->title = $request->input('title');
        //$static_page->resluggify();
        $static_page->content = $request->input('content');
        $static_page->meta_keywords = $request->input('meta_keywords');
        $static_page->meta_description = $request->input('meta_description');
        $static_page->status = $request->input('status');
        $static_page->save();

        return redirect(admin_url('static-pages'))->with('success', 'Your page has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
